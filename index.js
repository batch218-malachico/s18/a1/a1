
console.log("Hello Activity")


	
	// 1.  Create a function which will be able to add two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of the addition in our console.
	// 	-function should only display result. It should not return anything.

	function getSumOf5plus15(){
		let firstNumber = 5;
		let secondNumber = 15;
		console.log("Displayed sum of 5 and 15")
		console.log(firstNumber + secondNumber)

	}

	getSumOf5plus15();

	// 	Create a function which will be able to subtract two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of subtraction in our console.
	// 	-function should only display result. It should not return anything.

	function subtract20minus5(){
		let firstNumber = 20;
		let secondNumber = 5;
		console.log("Displayed difference of 20 and 5")
		console.log(firstNumber - secondNumber)

	}

	subtract20minus5();

	// 	-invoke and pass 2 arguments to the addition function
	// 	-invoke and pass 2 arguments to the subtraction function


	// 2.  Create a function which will be able to multiply two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the multiplication.


	// function returnProductof50and10(){
	// 	let firstNumber = 50;
	// 	let secondNumber = 10;
	// 	console.log("The product of 50 and 10:")
	// 	console.log(firstNumber * secondNumber)

	// }

	// returnProductof50and10();


	/*==================*/


	function returnProductof50and10(){
		
		console.log("The product of 50 and 10:");
		let productOf50and10 = 50 * 10;
		return productOf50and10;

	}

	let productOf = returnProductof50and10("The product of 50 and 10");
	console.log(productOf);


	// 	Create a function which will be able to divide two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the division.

	function returnQuotientof50and10(){

		console.log("The quotient of 50 and 10:");
		let quotientOfNumber = 50 / 5;
		return quotientOfNumber;
		
	}

	let quotientOf = returnQuotientof50and10("The quotient of 50 and 10:");
	console.log(quotientOf);

	// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
	// 		-a number should be provided as an argument.
	// 		-look up the formula for calculating the area of a circle with a provided/given radius.
	// 		-look up the use of the exponent operator.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the area calculation.

	function totalAreaOfCircle(){

		console.log("The result of getting the area of a circle with 15 radius:");
		let radiusOfCircle = 15;
		let diameterOfCircle = radiusOfCircle ** 2;
		let piFormula = Math.PI;
		let areaCircle = piFormula * diameterOfCircle;
		return areaCircle;

	}

	let circleArea = totalAreaOfCircle("The result of getting the area of a circle with 15 radius:");
	console.log(circleArea)

	// 	Create a global variable called outside of the function called circleArea.
	// 		-This variable should be able to receive and store the result of the circle area calculation.

	// Log the value of the circleArea variable in the console.

	// 4. 	Create a function which will be able to get total average of four numbers.
	// 		-4 numbers should be provided as an argument.
	// 		-look up the formula for calculating the average of numbers.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the average calculation.

	function averageOfNumbers(){

		console.log("The average of 20, 40, 60, and 80:");
		let randomNumber = 20 + 40 + 60 + 80;
		let totalCounts = 4;
		let theAverageIs = randomNumber / totalCounts;
		return theAverageIs;

	}

	let numberAverage = averageOfNumbers("The average of 20, 40, 60, and 80:")
	console.log(numberAverage)
	

	//     Create a global variable called outside of the function called averageVar.
	// 		-This variable should be able to receive and store the result of the average calculation
	// 		-Log the value of the averageVar variable in the console.

	

	function isMyScorePassed(){

		console.log("Is 38/50 a passingScore?");
		let myScore = 38;
		let totalScore = 50;
		let passingScore = .75 * 50;
		let passingPercentage = passingScore / totalScore * 100;
		let myScorePercentage = myScore / totalScore * 100;
		let boolean = myScorePercentage > passingPercentage;
		return boolean;

	}

	let theScore = isMyScorePassed("Is 38/50 a passingScore?");
	console.log(theScore)

	

	// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	// 		-this function should take 2 numbers as an argument, your score and the total score.
	// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
	// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	// 		-return the value of the variable isPassed.
	// 		-This function should return a boolean.


	// 	Create a global variable called outside of the function called isPassingScore.
	// 		-This variable should be able to receive and store the boolean result of the checker function.
	// 		-Log the value of the isPassingScore variable in the console.